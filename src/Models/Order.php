<?php
namespace Krasimird\AioraOrderService\Models;

use Illuminate\Database\Eloquent\Model;

class Order
{

    public $id;

    /**
     * Order constructor.
     * @param $id
     */
    public function __construct(){}


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setId($id)
    {
        $this->id = $id;
    }




}
