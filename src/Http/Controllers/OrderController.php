<?php
namespace Krasimird\AioraOrderService\Http\Controllers;

use Krasimird\AioraOrderService\Http\Requests\OrderPostRequest;
use Krasimird\AioraOrderService\Services\PaymentService;


class OrderController extends \Illuminate\Routing\Controller
{

    /**
     *
     * @OA\Post(
     *      path="/order",
     *      operationId="getProjectsList",
     *      tags={"Order"},
     *      summary="Create order",
     *      description="Create orde",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function create(OrderPostRequest $request)
    {

        $response = (new PaymentService($request))->process();

        return response()->json($response)->setStatusCode($response->getStatusCode());
    }


    /**
     *
     * @OA\Get(
     *      path="/order",
     *      operationId="getProjectsList",
     *      tags={"Order"},
     *      summary="sdadfdssdfsdfsdfsdfsdfsdfsfdsdf",
     *      description="List of orders",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     */
    public function listAll(OrderPostRequest $request)
    {

        return response()->json([]);
    }

}
