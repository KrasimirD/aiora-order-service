<?php
namespace Krasimird\AioraOrderService\Providers;

use Krasimird\AioraOrderService\Events\OrderCreated;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Krasimird\AioraOrderService\Listeners\OnOrderCreated;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        OrderCreated::class => [
            OnOrderCreated::class
        ]
    ];


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
