<?php
namespace Krasimird\AioraOrderService\Listeners;

use Krasimird\AioraOrderService\Events\OrderCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class OnOrderCreated implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \Digitall\Aiora\OrderService\Events\OrderCreated  $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {


    }

}
